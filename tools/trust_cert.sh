#!/bin/bash
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2019 AT&T Intellectual Property. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================
#
# What this is: Utility to add a trusted CA certificate to the set of trusted
# CAs for Graphene core components dependent upon CA trust (Federation). Typical
# use is to enable use of a self-signed server cert for a peered Graphene platform.
#
# Prerequisites:
# - Graphene AIO platform deployed, with access to the saved environment files
# - Run this script from the eclipse-graphene/tools folder on the Graphene host
# - Hostname/FQDN must be resolvable on the host where this script is run
#
# Usage:
# $ bash trust_cert.sh <cert> <alias>
#   cert: full path to cert file
#   alias: alias to associated with the cert (e.g. hostname)
#
# After running this script, restart the affected component e.g. federation.
#

function trust_cert() {
  trap 'fail' ERR
  cd $AIO_ROOT/certs/
  if [[ $(keytool -delete -alias $alias -keystore $GRAPHENE_TRUSTSTORE -storepass $GRAPHENE_TRUSTSTORE_PASSWORD -noprompt) ]]; then
    log "Prior alias in $GRAPHENE_TRUSTSTORE deleted"
  fi
  keytool -import \
    -file $cert \
    -alias $alias \
    -keystore $GRAPHENE_TRUSTSTORE \
    -storepass $GRAPHENE_TRUSTSTORE_PASSWORD -noprompt

  if [[ "$DEPLOYED_UNDER" == "docker" ]]; then
    cp $GRAPHENE_TRUSTSTORE /mnt/$GRAPHENE_NAMESPACE/certs/.
  else
    if [[ $(kubectl get configmap -n $GRAPHENE_NAMESPACE graphene-certs) ]]; then
      log "Delete existing graphene-certs configmap"
      kubectl delete configmap -n $GRAPHENE_NAMESPACE graphene-certs
    fi
    kubectl create configmap -n $GRAPHENE_NAMESPACE graphene-certs \
      --from-file=$GRAPHENE_KEYSTORE_P12,$GRAPHENE_TRUSTSTORE,$GRAPHENE_CA_CERT,$GRAPHENE_CERT
  fi
}

if [[ $# -lt 2 ]]; then
  cat <<'EOF'
 $ bash trust_cert.sh <cert> <alias>
   cert: full path to cert file
   alias: alias to associated with the cert (e.g. hostname)
EOF
  echo "All parameters not provided"
  exit 1
fi

set -x
trap 'fail' ERR
WORK_DIR=$(pwd)
cd $(dirname "$0")
export AIO_ROOT="$(cd ../AIO; pwd -P)"
source $AIO_ROOT/utils.sh
source $AIO_ROOT/graphene_env.sh
cert=$1
alias=$2
trust_cert
cd $WORK_DIR
