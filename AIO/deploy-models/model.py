#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2022 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import os
from time import time
from typing import List, Dict


def utf8len(s):
    """Reference Link: https://stackoverflow.com/a/30686735"""
    return len(s.encode('utf-8'))


default_tags = {
    "modelTypeCode": "DS",
    "toolkitTypeCode": "SK",
    "tags": [
        {
            "tag": "OpenML"
        }
    ]
}


class Model(object):
    """
    This class holds all the information for a graphene model.
    """

    def __init__(self,
                 model_name: str = 'tutorial-model',
                 docker_uri: str = '',
                 model_descrp: str = 'This is a model in tutorial group.',
                 category: str = 'Other',
                 documents: List = [],
                 tags: Dict = default_tags,
                 proto_buf_name="model.proto",
                 icon_filename: str = 'icon.jpg',
                 license_file_name="license.json",
                 local_fpath='.',
                 authors=None,
                 ) -> None:
        # These fields are required during model onboarding step
        self.base_model_name = model_name
        self.name = self.generate_new_model_name()
        print(f'Model Name: {self.name}')
        self.docker_uri = docker_uri
        self.proto_buf = proto_buf_name
        self.license = license_file_name

        # These fields are taken from the publication step
        self.description = model_descrp  # provides the model description
        self.license = license_file_name
        self.category = category
        self.documents_list = documents  # extra documents such as a README file or so on
        self.tags = tags
        self.icon = ''
        self.documents = {}
        user_id = os.environ['GRAPHENE_USERID']

        # Some additional fields that would be helpful
        self.local_fpath = local_fpath  # location of the model folder on current subsystem

        icon_fp = os.path.join(self.local_fpath, icon_filename)
        with open(icon_fp, 'rb') as icon_img:
            self.icon = icon_img.read()  # icon or thumbnail image for the model

        for doc_name in self.documents_list:
            doc_fp = os.path.join(self.local_fpath, doc_name)
            doc = None
            with open(icon_fp, 'rb') as doc_file:
                doc = doc_file.read()  # icon or thumbnail image for the model
                doc_size = utf8len(doc)
                self.documents[doc_name] = {
                    "name": doc_name,
                    "size": doc_size,
                    "uri": doc,
                    "userId": user_id
                }

        if len(self.name) == 0:
            # If the model name is empty
            self.name = os.path.basename(self.local_fpath)

        # Loading the associated files into the files dictionary
        self.onboarding_files = {}
        license_fp = os.path.join(self.local_fpath, self.license)
        proto_fp = os.path.join(self.local_fpath, self.proto_buf)
        self.license = open(license_fp, 'rb')
        self.proto_buf = open(proto_fp, 'rb')
        self.onboarding_files['license'] = ('license.json', self.license, 'application.json')
        self.onboarding_files['protobuf'] = ('model.proto', self.proto_buf, 'text/plain')

        default_author_name = "The Graphene Development Team"
        default_author_contact = "https://projects.eclipse.org/projects/technology.graphene"

        self.authors = authors
        if self.authors is None:
            self.authors = {
                "authors": [{
                    "name": default_author_name,
                    "contact": default_author_contact
                }],
                "publisher": default_author_name
            }

    def generate_new_model_name(self) -> str:
        """
        Generates unix timestamp and appends it to the end of the model name.
        This makes sure everytime the model is onboarded, it has an unique name.
        """
        curr_time = int(time())
        new_model_name = f"{self.base_model_name}_{curr_time}"
        return new_model_name
