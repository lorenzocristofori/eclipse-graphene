#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2022 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import os

from model import Model
import requests
import json
from typing import Dict
import numpy as np


def load_models_from_json(json_file_name: str = 'tutorial_models.json'):
    """
    Reads the details of all models to be onboarded and published from json file, and loads them into the memory.
    :param json_file_name: location of the json file.
    :return: dictionary with details of each model to be uploaded to the graphene host.
    """
    models_dict = {}
    tutorial_pipelines = json.load(open(json_file_name, 'r'))
    for pipeline in tutorial_pipelines:
        pipeline_loc = pipeline['location']
        pipeline_name = pipeline['pipeline_name']
        models = pipeline['models']
        for model in models:
            model_name = model['model_name']
            docker_uri = model['docker_uri']
            description = model['description']
            category = model['category']
            documents = model['documents']
            tags = model['tags']
            icon_filename = model['icon_filename']
            protobuf = model['protobuf']
            license = model['license']
            model_obj = Model(
                model_name,
                docker_uri,
                description,
                category,
                documents,
                tags,
                protobuf,
                icon_filename,
                license,
                os.path.join(pipeline_loc, model_name)
            )
            models_dict[model_name] = {'model': model_obj, 'pipeline': pipeline_name}
    return models_dict


class ModelUploader(object):
    """
    This class reads the model's configurations from a json file and onboards & publishes it to the specified host.
    It reads a lot of authentication related details from environment variables, make sure to set relevant details.
    The required environment variables are:
    - GRAPHENE_HOST
    - REGISTRY_HOST
    - GRAPHENE_TOKEN
    - GRAPHENE_USER
    - GRAPHENE_PW
    - GRAPHENE_CATALOGID
    - GRAPHENE_USERID
    """
    def __init__(
            self,
    ) -> None:
        # setup parameters from rading environment variables.
        self.GRAPHENE_HOST = os.environ['GRAPHENE_HOST']  # FQHN like aiexp-preprod.ai4europe.eu
        self.REGISTRY_HOST = os.environ['REGISTRY_HOST']
        self.GRAPHENE_TOKEN = os.environ['GRAPHENE_TOKEN']  # format is 'graphene_username:API_TOKEN'
        self.GRAPHENE_USER = os.environ['GRAPHENE_USER']
        self.GRAPHENE_PW = os.environ['GRAPHENE_PW']
        self.GRAPHENE_CATALOGID = os.environ['GRAPHENE_CATALOGID']
        self.GRAPHENE_USERID = os.environ['GRAPHENE_USERID']

        self.docker_base = f'{self.REGISTRY_HOST}:7444/ai4eu-experiments/openml'

        self.header_type = {
            "Content-Type": "application/json",
            "Authorization": self.GRAPHENE_TOKEN,
        }

    def update_data(self, api: str, data: Dict, headers: Dict, post_req: bool = True) -> bool:
        """
        This method makes the api call to perform activities related to model onboarding and publishing.
        :param api: url to make the api call.
        :param data: dictionary based data that needs to be sent as payload along with the request.
        :param headers: dictionary based header values for the api call.
        :param post_req: boolean flag to indicate if it's a POST api call.
        :return: boolean value, where `True` indicates the call was successful.
        """
        auth_data = (self.GRAPHENE_USER, self.GRAPHENE_PW)
        if post_req:
            update_response = requests.post(api,
                                            auth=auth_data,
                                            headers=headers,
                                            data=json.dumps(data)
                                            )
        else:
            if headers:  # for not image
                # get data first
                get_response = requests.get(api, auth=auth_data)
                if get_response.status_code != 200:
                    print(f'Fail to get data! Error code: {get_response.status_code}')
                    return False

                get_data = json.loads(get_response.text)
                # update data
                get_data.update(data)
                data = json.dumps(get_data)
            update_response = requests.put(api,
                                           auth=auth_data,
                                           headers=headers,
                                           data=data
                                           )

        if update_response.status_code == 201 and post_req:
            return True
        if update_response.status_code != 200:
            print(f'Fail: {update_response.status_code}')
            print(f'Response Text: {update_response.text}')
            return False
        return True

    def onboard_model(self, model: Model):
        """
        Onboards the model at the graphene host.
        :param model: Model object holds the details of the model to be onboarded.
        :return: dictionary object with relevant details of onboarded model such as `solution_id`.
        """
        # Setup parameters & call API to onboard model
        advanced_api = f"https://{self.GRAPHENE_HOST}:443/onboarding-app/v2/advancedModel"
        files = model.onboarding_files
        headers = {
            "Accept": "application/json",
            "modelname": model.name,
            "Authorization": self.GRAPHENE_TOKEN,
            "dockerFileURL": model.docker_uri,
            'isCreateMicroservice': 'false',
            'description': 'Description from the header!'
        }

        # send request
        response = requests.post(advanced_api, files=files, headers=headers)

        # check response
        if response.status_code == 201:
            # Retrieve relevant details from the response
            body = json.loads(response.text)
            solution_id: str = body['result']['solutionId']
            onboarding_resp: Dict = {
                'solution_id': solution_id,
                'task_id': body['taskId'],
                'tracking_id': body['trackingId'],
                'user_id': body['result']['userId']
            }

            print(
                f"Docker uri is pushed successfully on {self.GRAPHENE_HOST},  response is: {response.status_code}, solution id: {solution_id}\n")
            return onboarding_resp

        print(f"Docker uri is not pushed on {self.GRAPHENE_HOST}, response is: {response.status_code}, \n")
        return False

    def set_authors_publisher(self, model: Model, solution_id: str, revision_id: str) -> bool:
        """
        This method calls an api to set the author and publisher details of the onboarded model.
        This is one of the pre-publication step.
        :param model: holds required details of the onboarded model.
        :param solution_id: solution identifier for the onboarded model.
        :param revision_id: current revision number for which we need to set the author and publisher details.
        :return: boolean value, where `True` indicates the call was successful.
        """
        author_api = f'https://{self.GRAPHENE_HOST}/ccds/solution/{solution_id}/revision/{revision_id}'
        status = self.update_data(author_api, model.authors, headers=self.header_type, post_req=False)
        print(f'Set Authors Status: {status}')
        return status

    def set_description(self, model: Model, solution_id: str, revision_id: str, catalog_id: str) -> bool:
        """
        This method calls an api to set the model description details of the onboarded model.
        This is one of the pre-publication step.
        :param model: holds required details of the onboarded model.
        :param solution_id: solution identifier for the onboarded model.
        :param revision_id: current revision number for which we need to set the description details.
        :return: boolean value, where `True` indicates the call was successful.
        """
        desc_api = f'https://{self.GRAPHENE_HOST}/ccds/revision/{revision_id}/catalog/{catalog_id}/descr'
        data = {
            "description": model.description
        }
        set_descrp_status = self.update_data(desc_api, data, headers=self.header_type, post_req=True)
        print(f'Setup Description Status: {set_descrp_status}')
        return set_descrp_status

    def set_tags(self, model: Model, solution_id: str, revision_id: str) -> bool:
        """
        This method calls an api to set the tags of the onboarded model.
        This is one of the pre-publication step.
        :param model: holds required details of the onboarded model.
        :param solution_id: solution identifier for the onboarded model.
        :param revision_id: current revision number for which we need to set the tags.
        :return: boolean value, where `True` indicates the call was successful.
        """
        tag_api = f'https://{self.GRAPHENE_HOST}/ccds/solution/{solution_id}'
        data = model.tags
        response = self.update_data(tag_api, data=data, headers=self.header_type, post_req=False)
        if response:
            tag_api = f'https://{self.GRAPHENE_HOST}/ccds/catalog/{self.GRAPHENE_CATALOGID}/solution/{solution_id}'
            status = self.update_data(tag_api, data=data, headers=self.header_type, post_req=True)
            print(f'Set Tags Status: {status}')
            return status
        print(f'Set Tags Status: {False}')
        return False

    def set_icon(self, model: Model, solution_id: str) -> bool:
        """
        This method calls an api to set the icon of the onboarded model.
        This is one of the pre-publication step.
        :param model: holds required details of the onboarded model.
        :param solution_id: solution identifier for the onboarded model.
        :return: boolean value, where `True` indicates the call was successful.
        """
        icon_api = f'https://{self.GRAPHENE_HOST}/ccds/solution/{solution_id}/pic'
        status = self.update_data(icon_api, data=model.icon, headers=None, post_req=False)
        print(f'Set Icon Status: {status}')
        return status

    def get_revision_id(self, solution_id: str):
        """
        This method calls an api to get the latest revision id of the onboarded model.
        :param solution_id: solution identifier for the onboarded model.
        :return: the latest revision number for the onboarded model.
        """
        revision_api = f'https://{self.GRAPHENE_HOST}/ccds/solution/{solution_id}/revision'
        response = requests.get(revision_api, auth=(self.GRAPHENE_USER, self.GRAPHENE_PW))
        if response.status_code == 200:
            body = json.loads(response.text)
            if body:
                version = np.array([i['version'] for i in body]).argmax()
                return body[version]['revisionId']
        else:
            print(f'Can not get Revision ID. Error code: {response.status_code}')
            return 0

    def pre_publication_steps(self, model: Model, solution_id: str, revision_id: str) -> bool:
        """
        This method calls the four pre-publication steps to set the model's:
        - Authors' and publishers' details.
        - Tags.
        - Description.
        - Icon file.
        :param model: holds required details of the onboarded model.
        :param solution_id: solution identifier for the onboarded model.
        :param revision_id: current revision number for which we need to set the required details.
        :return: boolean value, where `True` indicates all the calls were successful.
        """
        auto_status = self.set_authors_publisher(model, solution_id, revision_id)
        tags_status = self.set_tags(model, solution_id, revision_id)
        desc_status = self.set_description(model, solution_id, revision_id, self.GRAPHENE_CATALOGID)
        icon_status = self.set_icon(model, solution_id)

        return auto_status and tags_status and desc_status and icon_status

    def publish_model_approval_request(self, model: Model) -> object:
        """
        This method onboards and publishes the model.
        :param model: holds required details of the onboarded model.
        :return: boolean value, where `True` indicates the model was published successfully.
        """
        onboarding_resp = self.onboard_model(model=sample_model)
        if onboarding_resp:
            solution_id = onboarding_resp['solution_id']
            task_id = onboarding_resp['task_id']
            tracking_id = onboarding_resp['tracking_id']
            user_id = onboarding_resp['user_id']
            revision_id = self.get_revision_id(solution_id)
            pre_publish_result = self.pre_publication_steps(model, solution_id, revision_id)
            if pre_publish_result:
                publish_url = f"https://{self.GRAPHENE_HOST}/ccds/pubreq"
                payload = {
                    "catalogId": self.GRAPHENE_CATALOGID,
                    "reviewUserId": self.GRAPHENE_USERID,
                    "requestUserId": self.GRAPHENE_USERID,
                    "revisionId": revision_id,
                    "solutionId": solution_id,
                    "statusCode": "AP"
                }
                pub_resp = self.update_data(publish_url, payload, headers=self.header_type, post_req=True)
                return pub_resp
        else:
            print('One of the pre-publication steps failed, please try again with correct configurations')
            return False


if __name__ == '__main__':
    models = load_models_from_json(json_file_name='tutorial_models.json')
    mod_upl = ModelUploader()
    model_keys=models.keys()
    for model_key in model_keys:
        sample_model = models[model_key]['model']
        pub_resp = mod_upl.publish_model_approval_request(sample_model)
        print(pub_resp)
